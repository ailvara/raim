<!DOCTYPE HTML>
<html>
	<head>
		<?php include('scripts/header.php');?>
		<link rel="stylesheet" href="styles/cart_style.css" />
	</head>
	<body>
		<?php include('scripts\layout.php')?>
		<div id="main">
			<?php include('scripts\load_cart.php')?>
		</div>		
	</body>
</html>