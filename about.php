<!DOCTYPE HTML>
<html>
	<head>
		<?php include('scripts/header.php');?>
	</head>
	<body>
		<?php include('scripts\layout.php')?>
		<div id="main"><div id="inner">
			Witryna jest przygotowywana w ramach łączonego projektu z przedmiotów: <ul>
			<li>Rozwój aplikacji internetowych w medycynie</li>
			<li>Hurtownie i eksploracja danych</li>
			</ul></br>
			<strong>Nic tu tak naprawdę nie sprzedajemy</strong><br><br><hr>
			<ol>
			<h4><li>Temat projektu:</li></h4> 
			<p>Sklep internetowy</p>
			<h4><li>Cel projektu:</li></h4> 
			<p>Celem projektu jest stworzenie przykładowego (fikcyjnego) sklepu internetowego oferującego gadżety z nadrukami 
			oraz zgromadzenie wiedzy na temat decyzji podejmowanych przez klientów.</p>
			<h4><li>Efekty projektu:</li></h4> 
			<p>Efektem projektu powinna być witryna - sklep internetowy, składająca się z:
			<ul>
			<li>plików źródłowych (HTML i CSS, skrypty PHP, JavaScript)</li>
			<li>bazy danych</li>
			<li>oraz wiedza wygenerowana na podstawie zebranych danych o "sprzedaży"</li>
			</ul></p></ol><br><hr>
			<h4>O nas:</h4>
			...
			
		</div></div>
		
	</body>
</html>