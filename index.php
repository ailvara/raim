﻿<!DOCTYPE HTML>
<html>
	<head>
		<?php include('scripts/header.php');?>
		<link rel="stylesheet" href="styles/index_content.css" />
		
	</head>
	<body>
		<?php include('scripts\layout.php')?>
		<div id="main">
			<div id="intro">
				<h3>Słowem wstępu...</h3>
				<p>To <strong>nie jest sklep</strong>. Stronka jest projektem, który (między innymi) ma na celu, aby zebrać dane o tym, co by się tu działo, gdyby faktycznie można było coś kupić. 
				Szczegółów nie zdradzimy, żeby nic nie Wam nie sugerować... W każdym razie byłybyśmy wdzięczne, gdybyście uruchomili na chwilę wyobraźnię 
				i <strong>złożyli udawane zamówienie</strong> na printy, które Wam się najbardziej spodobają.</p>
				<p>Printy to np. plakaty, nadruki na koszulki, torby itd. Tu forma jest niesprecyzowana, możecie sobie wymyślić, gdzie chcielibyście mieć taki nadruk.
				</p>
				<p>Podczas logowania prosimy <strong>nie podawać loginów i haseł używanych gdzie indziej</strong>. Ochrona i szyfrowanie danych nie są przedmiotem tego projektu.
				Wszelkie dane osobowe również możecie zmyślić w granicach rozsądku. Albo zabawić się w mały role-play (co by kupiła moja babcia?). Im więcej pojawi się opcji,
				tym będziemy szczęśliwsze.</p>
				<p>Jeśli dotarliście z czytaniem aż tu, gratulacje i zapraszamy dalej;)</p>
			</div>
			<h3>Szczególnie polecamy:</h3>
			<?php include('scripts/index_load.php') ?>
		</div>		
		
		</div>		
	</body>
</html>