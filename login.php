<!DOCTYPE HTML>
<html>
	<head>
		<?php include('scripts/header.php');?>
		<link rel="stylesheet" href="styles/accounts.css">
		<script src="scripts/form_validation.js"></script>
	</head>
	<body>
		<?php include('scripts\layout.php')?>
		<div id="main">
			<div id="log_form"><div class="inner">
				Masz już u nas konto?
				<h2>Zaloguj się</h2>
				<?php include('scripts\login.php');?>
			</div></div>
			<div id="reg_form"><div class="inner">
				Jesteś tu pierwszy raz?
				<h2>Zarejestruj się</h2>
				<?php include('scripts\register.php');?>
			</div></div>
		</div>		
	</body>
</html>