�eby otworzy� stron� z dropa, po instalacji xampp w pliku httpd.conf w obr�bie znacznika <IfModule alias_module> trzeba uzupe�ni� i wklei� kod:

<Directory "adres_dropboxa/RAIM">
   Require all granted
</Directory>
Alias /sklep "adres_dropboxa/RAIM"

Sklep znajdzie si� pod adresem localhost/sklep.

-----------------------------------------------------------

Baz� danych robimy w phpMyAdmin -> MySQL (dost�p z menu po lewej na stronie localhost/xampp). Projekt trzeba dogra�, na razie do tych skrypt�w co s� starczy tyle.

Dane bazy:
nazwa: shop
login: root
has�o: pass
tabela: products

Elementy products (jak na razie):
id (int)
name (varchar(40))
descr (text)
price (decimal(6,2))
promoted (int)
category(varchar(10))
popularity (int)

Przydadz� si� minimum dwa elementy nazwane Example 1 i Example 2 do prezentacji.

--------------------------------------------------------------

Good luck, poprzegl�dajcie, ogarnijcie i ruszamy z kopytek.