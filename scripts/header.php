<?php

	echo ('
	<meta charset="UTF-8"/>
	<meta name="title" content="TrialShop" />
	<link rel="stylesheet" href="styles/fonts.css" />
	');
	
	if(!isset($_SESSION)) session_start();
	if(isset($_GET['stylesheet'])) {
		$_SESSION['style'] = $_GET['stylesheet'];
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}
	
	if(!isset($_SESSION['style'])) $_SESSION['style']="modern";
	if($_SESSION['style']=="modern") echo('<link rel="stylesheet" href="styles/main_style.css" />');
	else echo('<link rel="stylesheet" href="styles/main_style_old.css" />');

	echo('
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
	<script type="text/javascript" src="scripts/dropdown.js"> </script>
	<script type="text/javascript" src="scripts/login_validation.js"></script>
	');
	
?>