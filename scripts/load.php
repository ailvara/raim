<?php 
/*
* Skrypt pobierający produkty z bazy danych.
* Do zrobienia: Informacja o braku wyników
*/

	$hostname = 'localhost';
	$username = 'root';
	$password = 'pass';
	$link = @mysql_connect($hostname, $username, $password);
	$database = 'shop';
	
	if(is_resource($link)) {
		if(mysql_select_db($database, $link)) {
		
			//Tworzenie zapytania do bazy danych
			//$sql='SELECT name, LEFT(descr, 40), promoted, popularity, category, price FROM products';
			$sql='SELECT * FROM products';
			
			//Dodanie filtrów
			if(isset($_GET['category']) || isset($_GET['price'])) $sql=$sql.' WHERE';
			if(isset($_GET['category'])) {
				$sql=$sql." category='".$_GET['category']."'";
				if(isset($_GET['price'])) $sql=$sql.' AND ';
			}
			if(isset($_GET['price'])) {
				$sql=$sql." price<=".$_GET['price'];
			}
			
			//Dodanie sortowania
			$sql = $sql.' ORDER BY promoted DESC';
			if(isset($_GET['sort'])){
				if($_GET['sort']=="popularity") $sql=$sql.', '.$_GET['sort'].' DESC';
				else $sql=$sql.', '.$_GET['sort'];
				//echo($sql);
			}
			
			//Pobranie danych
			$data = mysql_query($sql);
			
			//Wyświetlenie wyników
			$how_many=0;
			while($result=mysql_fetch_array($data)) {
				$how_many++;
				if($result['promoted']==1) {
					echo '<div class="promoteditem"><h3>' , $result['name'] , '<span class="price">',  $result['price'], '</span></h3>';
				}
				else {
					echo '<div class="item"><h3>' , $result['name'] , '<span class="price">',  $result['price'], '</span></h3></h3>';
				}
				$src = str_replace(' ', '+', $result['name']);
				echo '<img src="ld_prints/'.$src.'.png" class="image">';
				$abridged_descr = substr($result['descr'], 0, 170).'...';
				echo '<div class="descr">', $abridged_descr;
				echo '<div class="buttons"><a href="more.php?prod='.$src.'"><button class="seemore">Zobacz wiecej</button></a>';
				echo '<a href="cart.php?action=add&item='.$result['id'].'"><button class="tobasket">Do koszyka</button></a></div></div></div>';
			}
			if($how_many==0) echo 'Brak wyników';
		}
		mysql_close($link);
	} else { echo 'Error occured while connecting to database' ;}
	
?>