/*
* Skrypty odpowiadające za walidację formularza rejestracji
* Dodać walidację długości loginu i hasła max 20 znaków
*/

//Additionally check form when sending
function validateForm() {

	var login=document.forms["register"]["login"].value;
	if (login==null || login=="" || login.length>20) {
		alert("Musisz podać login! (maksymalnie 20 znaków)");
		return false;
	}
	
	var pass1=document.forms["register"]["pass1"].value;
	if (pass1==null || pass1=="" || pass1.length>20) {
		alert("Musisz podać hasło! (maksymalnie 20 znaków)");
		return false;
	}
	
	var pass2=document.forms["register"]["pass2"].value;
	if (pass1!=pass2) {
		alert("Hasła się nie zgadzają.");
		return false;
	}

}

//Check login when changed
function checkLogin() {

	var login = document.getElementById("form_login");
	var l = login.value;
	if(l.length==0) { 
		login.style.border="2px solid red";
	}
	else if(l.length>20){
		login.style.border="2px solid red";
		alert('Login może mieć maksymalnie 20 znaków.');
	}
	else {
		login.style.border="2px solid green";
	}
	enable();
}

//Check password when changed
function checkPassword() {

	var password = document.getElementById("form_pass1");
	var p = password.value;
	if(p.length==0) { 
		password.style.border="2px solid red";
	}
	else if(p.length>20){
		password.style.border="2px solid red";
		alert('Hasło może mieć maksymalnie 20 znaków.');
	}
	else {
		password.style.border="2px solid green";
	}
	checkRetypedPassword();
	enable();
	
}

//Check password confirmation when changed
function checkRetypedPassword() {

	var password = document.getElementById("form_pass1");
	var retyped = document.getElementById("form_pass2");
	var pass1 = password.value;
	var pass2 = retyped.value;
	
	if(pass1==pass2) retyped.style.border="2px solid green";
	else  retyped.style.border="2px solid red";
	enable();
	
}

//Enable submit button if all is ok
function enable(){
	var good = true;
	if($("#form_login").css("border-left-color")!="rgb(0, 128, 0)") good = false;
	if($("#form_pass1").css("border-left-color")!="rgb(0, 128, 0)") good = false;
	if($("#form_pass2").css("border-left-color")!="rgb(0, 128, 0)") good = false;
	if(good==true) document.getElementById("register").disabled = false;
	else document.getElementById("register").disabled = true;
}