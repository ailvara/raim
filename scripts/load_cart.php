<?php
if(!isset($_SESSION)){
    header('Location: login.php');
}

if(!isset($_SESSION['logged'])) header('Location: login.php');
else if($_SESSION['logged'] == false) header('Location: login.php');
else {

if(isset($_GET['action'])){

	$action = $_GET['action'];
	if(isset($_GET['item'])) $item = $_GET['item'];

	switch($action){
		case 'add':
			if(isset($_SESSION['cart'][$item])) $_SESSION['cart'][$item]++;
			else $_SESSION['cart'][$item]=1;
			break;
		case 'addone':
			$_SESSION['cart'][$item]++;
			break;
		case 'minone':
			$_SESSION['cart'][$item]--;
			if($_SESSION['cart'][$item]==0) unset($_SESSION['cart'][$item]);
			break;
		case 'remove':
			unset($_SESSION['cart'][$item]);
			break;
		case 'empty':
			unset($_SESSION['cart']);
			break;
		default: echo('Wystąpił błąd.');
	}
	
	if(empty($_SESSION['cart'])) unset($_SESSION['cart']);
	header('Location: cart.php');
}

else {

	echo('<h4><a href="javascript:history.back()" id="back">< Powrót</a></h4><h2>Koszyk</h2>');
	if(!isset($_SESSION['cart'])) echo('Koszyk jest pusty');
	
	else {

	$hostname = 'localhost';
	$username = 'root';
	$password = 'pass';
	$link = @mysql_connect($hostname, $username, $password);
	$database = 'shop';
	
	$all_sum=0;
	
	if(is_resource($link)) {
		if(mysql_select_db($database, $link)) {
		echo('<hr><table id="list">');
			foreach($_SESSION['cart'] as $id => $quantity) {
				echo('<tr>');
				$sql = 'SELECT * FROM products WHERE id="'.$id.'"';
				$data = mysql_query($sql);
			    $result=mysql_fetch_array($data);
				echo ('<td><img src="ld_prints/'.str_replace(' ', '+', $result['name']).'.png" class="icon"></td>');
				echo ('<td><h3><a href="more.php?prod='.str_replace(' ', '+', $result['name']).'">'.$result['name'].'</a></h3></td>');
				$sum = $result['price']*$quantity;
				$all_sum+=$sum;
				echo ('<td>x'.$quantity.'</td><td>'.$sum.'zł</td>');
				echo ('<td class="biggerfont"><a href="cart.php?action=addone&item='.$id.'">+</a>');
				echo ('<td class="biggerfont"><a href="cart.php?action=minone&item='.$id.'">-</a>');
				echo ('<td><a href="cart.php?action=remove&item='.$id.'">usuń</a></td>');
				echo ('</tr>');
			}
				
			echo ('</table><hr>');
		}
		
	} else echo 'Error occured while connecting to database';

	echo ('<div id="clear"><h3>Suma: '.$all_sum.'zł</h3><br><a href="cart.php?action=empty">Opróżnij koszyk</a></div>');
	echo ('<div id="buy"><a href="confirm.php">Ok, kupuję!</a></div>');
	
	mysql_close($link);
	
	
}
}
}

?>