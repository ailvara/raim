<?php
/*
* Skrypt tworzący filtry w lewym pasku bocznym. 
* Do zrobienia: przedziały cen; 
* Opcjonalnie: czyszczenie selektywne, przeróbka na AJAX.
*/

/*Categories*/
echo('<h4>Kategorie</h4><ul><li>');
new_category('Zwierzaki','animals');
echo('<li>');
new_category('Napisy','text');
echo('<li>');
new_category('Zdjęcia','foto');
echo('</ul>');

/*Prices*/
echo('<h4>Ceny do:</h4><ul><li>');
new_price('20');
echo('<li>');
new_price('30');
echo('<li>');
new_price('50');
//echo('<li>');
//new_price('Wyczyść');
echo('</ul>');

echo('<a href="browse.php">Czyść wszystko</a>');

function new_category($name, $new_cat){
	echo('<a href="browse.php?');
	if(isset($_GET['price'])) echo('price='.$_GET['price'].'&');
	if(isset($_GET['sort'])) echo('sort='.$_GET['sort'].'&');
	echo('category='.strtolower($new_cat));
	if(isset($_GET['category'])) {
		if(strtolower($new_cat)==$_GET['category']) echo('"><span class="chosen">'.$name.'</span></a>');
		else echo('">'.$name.'</a>');
	}
	else echo('">'.$name.'</a>');
}

function new_price($new_pri){
	echo('<a href="browse.php');
	echo('?price='.strtolower($new_pri));
	if(isset($_GET['sort'])) echo('&sort='.$_GET['sort']);
	if(isset($_GET['category'])) echo('&category='.$_GET['category']);
	if(isset($_GET['price'])) {
		if(strtolower($new_pri)==$_GET['price']) echo('"><span class="chosen">'.$new_pri.' zł</span></a>');
		else echo('">'.$new_pri.' zł</a>');
	}
	else echo('">'.$new_pri.'</a>');
}

?>