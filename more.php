<!DOCTYPE HTML>
<html>
	<head>
		<?php include('scripts/header.php');?>
		<link rel="stylesheet" href="styles/item_style.css" />
	</head>
	<body>
		<?php include('scripts\layout.php')?>
		<div id="main">
			<h4><a href="javascript:history.back()" id="back">< Powrót</a></h4>
			<?php include('scripts\item.php');?>
		</div>		
	</body>
</html>