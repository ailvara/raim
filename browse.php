﻿<!DOCTYPE HTML>
<html>
	<head>
		<?php include('scripts/header.php');?>
		<link rel="stylesheet" href="styles/list_style.css" />
	</head>
	<body>
		<?php include('scripts\layout.php');?>
		<div id="main">
			<div id="sidebar">
				<h3>Filtruj według</h3>
				<hr><?php include('scripts\filter.php');?><hr>
			</div>
			<div id="orderby"><?php include('scripts/sort.php');?></div>
			<div id="content"><?php include('scripts/load.php');?></div>		
		</div>		
	</body>
</html>